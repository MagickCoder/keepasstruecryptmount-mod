﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
namespace KeePassTrueCryptMount
{
    using KeePass.Plugins;
    using KeePass.UI;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    public partial class TrueCryptOptionsForm : Form
    {
        private IPluginHost pluginHost;

        public TrueCryptOptionsForm(IPluginHost host)
        {
            this.pluginHost = host;

            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            this.Icon = Icon.FromHandle(Resources.TrueCryptNormal.GetHicon());

            this.picBanner.BackgroundImage = BannerFactory.CreateBanner(
                this.picBanner.Width,
                this.picBanner.Height,
                BannerStyle.Default,
                Resources.B48_TrueCrypt,
                LanguageTexts.BannerPluginTitleText,
                LanguageTexts.BannerOptionsDescriptionText);

            this.txtTrueCryptExecutable.Text = this.pluginHost.GetTrueCryptExecutable();
            this.chkShowMenuItemAlways.Checked = this.pluginHost.GetTrueCryptMenuItemAlwaysVisible();
            this.numericAutoTypeTimeout.Value = this.pluginHost.GetTrueCryptAutoTypeWaitTimeout();

            this.OnTrueCryptPathTextChanged(this, e);

            GlobalWindowManager.AddWindow(this);

            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            GlobalWindowManager.RemoveWindow(this);

            base.OnClosing(e);
        }

        private void OnButtonOpenFileDialogClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtTrueCryptExecutable.Text))
            {
                try
                {
                    this.ofdTrueCryptExecutable.FileName = Path.GetFileName(this.txtTrueCryptExecutable.Text);
                    this.ofdTrueCryptExecutable.InitialDirectory = Path.GetDirectoryName(this.txtTrueCryptExecutable.Text);
                }
                catch (ArgumentException)
                {
                    Debug.WriteLine(string.Format("KeePassTrueCryptMount: Path contains invalid chars -> {0}", this.txtTrueCryptExecutable.Text));
                }
                catch (PathTooLongException)
                {
                    Debug.WriteLine(string.Format("KeePassTrueCryptMount: Path is to long -> {0}", this.txtTrueCryptExecutable.Text));
                }
            }

            if (this.ofdTrueCryptExecutable.ShowDialog(this) == DialogResult.OK)
            {
                this.txtTrueCryptExecutable.Text = this.ofdTrueCryptExecutable.FileName;
            }
        }

        private void OnOkButtonClicked(object sender, EventArgs e)
        {
            this.pluginHost.SetTrueCryptExecutable(this.txtTrueCryptExecutable.Text);
            this.pluginHost.SetTrueCryptMenuItemAlwaysVisible(this.chkShowMenuItemAlways.Checked);
            this.pluginHost.SetTrueCryptAutoTypeWaitTimeout((int)this.numericAutoTypeTimeout.Value);
        }

        private void OnTrueCryptPathTextChanged(object sender, EventArgs e)
        {
            var executableOk = TrueCryptInfo.ExecutableExists(this.txtTrueCryptExecutable.Text);

            this.txtTrueCryptExecutable.BackColor = executableOk
                ? SystemColors.Window 
                : Color.Coral;

            this.btnOk.Enabled = executableOk;
        }

        private void OnRegistryResolveButtonClicked(object sender, EventArgs e)
        {
            this.txtTrueCryptExecutable.Text = TrueCryptInfo.ResolveExecutableFromRegistry();
        }

        private void OnDonateLabelLinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // start the prefered browser with paypal.
            var donationUri = @"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=N6FQKZTAX87ZQ";
            ProcessStartInfoExtensions.Execute(donationUri);
        }

        private void OnHelpLabelLinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var helpUri = @"https://bitbucket.org/schalpat/keepasstruecryptmount/wiki/Home";
            ProcessStartInfoExtensions.Execute(helpUri);
        }
    }
}
