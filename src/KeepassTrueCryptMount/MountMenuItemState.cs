﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassTrueCryptMount
{
    using KeePassLib;
    using System;
    using System.Windows.Forms;

    public class MountMenuItemState
    {
        /* 
         * The states matrix
         * 
         *  Entry         SHIFT
         *  Active  Hide  Hold           Result
         *    1      0      0        VisibleWithMountDialog
         *    1      1      0        VisibleWithoutMountDialog
         *    1      0      1        VisibleWithoutMountDialog
         *    1      1      1        VisibleWithMountDialog
         *    0      x      0        Invisible
         *    0      x      1        VisibleWithMountDialog
         *    
         *  The Propery AlwaysVisible (controlled by the Options checkbox)
         *  results always in VisibleWithMountDialog.
         *  
         *  When selectedItemCount != 1 -> Invisible
        */
        private ToolStripMenuItem menuItem;

        public MountMenuItemState(ToolStripMenuItem menuItem)
        {
            this.menuItem = menuItem;
        }

        public bool ShouldMountDialogShown
        {
            get { return this.Current == MountMenuItemStates.AlwaysVisible || this.Current == MountMenuItemStates.VisibleWithMountDialog; }
        }

        public bool AlwaysVisible { get; set; }

        public MountMenuItemStates Current { get; private set; }

        public void ChangeState(MountMenuItemStates state)
        {
            if (this.AlwaysVisible)
            {
                state = MountMenuItemStates.AlwaysVisible;
            }

            switch (state)
            {
                case MountMenuItemStates.AlwaysVisible:
                case MountMenuItemStates.VisibleWithMountDialog:
                    this.menuItem.Visible = true;
                    this.menuItem.Text = LanguageTexts.TCMountMenuItemText + LanguageTexts.MenuItemOpenDialogSuffix;
                    break;

                case MountMenuItemStates.VisibleWithoutMountDialog:
                    this.menuItem.Visible = true;
                    this.menuItem.Text = LanguageTexts.TCMountMenuItemText;
                    break;

                case MountMenuItemStates.Invisible:
                    this.menuItem.Visible = false;
                    break;
            }

            this.Current = state;
        }

        public MountMenuItemStates GetState(uint selectedItemCount, PwEntry entry, bool shiftHold)
        {
            return GetState(selectedItemCount, entry.HasMountSettings(), entry.ShouldHideMountDialog(), shiftHold);
        }

        public MountMenuItemStates GetState(uint selectedItemCount, bool entryActive, bool entryHide, bool shiftHold)
        {
            if (selectedItemCount == 0 || selectedItemCount > 1)
            {
                return MountMenuItemStates.Invisible;
            }

            if (!entryActive)
            {
                return shiftHold ? MountMenuItemStates.VisibleWithMountDialog : MountMenuItemStates.Invisible;
            }

            if ((entryHide && shiftHold) || (!entryHide && !shiftHold))
            {
                return MountMenuItemStates.VisibleWithMountDialog;
            }
            else
            {
                return MountMenuItemStates.VisibleWithoutMountDialog;
            }
        }
    }

    public enum MountMenuItemStates
    {
        AlwaysVisible,

        Invisible,

        VisibleWithMountDialog,

        VisibleWithoutMountDialog,
    }
}
