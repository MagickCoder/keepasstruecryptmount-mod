﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassTrueCryptMount
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Windows.Forms;
    
    public static class ProcessStartInfoExtensions
    {
        public static void ExecuteAndWait(this ProcessStartInfo info)
        {
            if (info == null)
            {
                return;
            }

            try
            {
                var process = Process.Start(info);

                if (process != null)
                {
                    process.WaitForExit();
                }
                else
                {
                    Console.WriteLine("Process could not start.");
                }
            }
            catch (InvalidOperationException ex)
            {
                ShowInvalidOperationExceptionMessageBox(ex);
            }
            catch (FileNotFoundException)
            {
                ShowFileNotFoundExceptionMessageBox(info.FileName);
            }
            catch (Win32Exception ex)
            {
                ShowWin32ExceptionMessageBox(ex);
            }
            catch (SystemException)
            {
                // maybe process execution is too fast for WaitForExit.
            }
        }

        public static void Execute(string fileNameOrUri)
        {
            Execute(new ProcessStartInfo(fileNameOrUri));
        }

        public static void Execute(this ProcessStartInfo info)
        {
            if (info == null)
            {
                return;
            }

            try
            {
                Process.Start(info);
            }
            catch (InvalidOperationException ex)
            {
                ShowInvalidOperationExceptionMessageBox(ex);
            }
            catch (FileNotFoundException)
            {
                ShowFileNotFoundExceptionMessageBox(info.FileName);
            }
            catch (Win32Exception ex)
            {
                ShowWin32ExceptionMessageBox(ex);
            }
        }

        public static ProcessStartInfo HideExecution(this ProcessStartInfo info)
        {
            info.WindowStyle = ProcessWindowStyle.Hidden;
            info.CreateNoWindow = true;
            info.UseShellExecute = false;

            return info;
        }

        public static ProcessStartInfo WithCommandLine(this ProcessStartInfo info, string arguments)
        {
            info.Arguments = arguments;
            return info;
        }

        private static void ShowInvalidOperationExceptionMessageBox(InvalidOperationException exception)
        {
            MessageBox.Show(
                string.Format("TrueCrypt process could not start.{0}{1}", Environment.NewLine, exception.Message),
                "InvalidOperationException",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
        }

        private static void ShowFileNotFoundExceptionMessageBox(string fileName)
        {
            MessageBox.Show(
                string.Format("TrueCrypt executable was not found.{0}{1}", Environment.NewLine, fileName),
                "FileNotFoundException",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
        }

        private static void ShowWin32ExceptionMessageBox(Win32Exception exception)
        {
            MessageBox.Show(
                string.Format("TrueCrypt process could not start.{0}{1}", Environment.NewLine, exception.Message),
                "Win32Exception",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
        }
    }
}
