﻿namespace KeePassTrueCryptMount
{
    partial class TrueCryptMountForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bannerPanel = new System.Windows.Forms.Panel();
            this.m_lblSeparator = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.mountOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.mountOptionsLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.readonlyCheckBox = new System.Windows.Forms.CheckBox();
            this.removableCheckBox = new System.Windows.Forms.CheckBox();
            this.backgroundCheckBox = new System.Windows.Forms.CheckBox();
            this.silentCheckBox = new System.Windows.Forms.CheckBox();
            this.beepCheckBox = new System.Windows.Forms.CheckBox();
            this.explorerCheckBox = new System.Windows.Forms.CheckBox();
            this.askPasswordCheckBox = new System.Windows.Forms.CheckBox();
            this.volumeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.driveComboBox = new System.Windows.Forms.ComboBox();
            this.tipInfo = new System.Windows.Forms.ToolTip(this.components);
            this.btnReloadDrives = new System.Windows.Forms.Button();
            this.hideDialogCheckBox = new System.Windows.Forms.CheckBox();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.stateToolStripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnApply = new System.Windows.Forms.Button();
            this.tipWarning = new System.Windows.Forms.ToolTip(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.keyFilesTextBox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.useUrlFieldCheckBox = new System.Windows.Forms.CheckBox();
            this.volumeFileGroupBox = new System.Windows.Forms.GroupBox();
            this.mountOptionsGroupBox.SuspendLayout();
            this.mountOptionsLayoutPanel.SuspendLayout();
            this.volumeFileGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // bannerPanel
            // 
            this.bannerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.bannerPanel.Location = new System.Drawing.Point(0, 0);
            this.bannerPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bannerPanel.Name = "bannerPanel";
            this.bannerPanel.Size = new System.Drawing.Size(640, 86);
            this.bannerPanel.TabIndex = 0;
            // 
            // m_lblSeparator
            // 
            this.m_lblSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_lblSeparator.Location = new System.Drawing.Point(4, 443);
            this.m_lblSeparator.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.m_lblSeparator.Name = "m_lblSeparator";
            this.m_lblSeparator.Size = new System.Drawing.Size(633, 2);
            this.m_lblSeparator.TabIndex = 7;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(528, 449);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(420, 449);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 16;
            this.btnOK.Text = "&Mount";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // mountOptionsGroupBox
            // 
            this.mountOptionsGroupBox.Controls.Add(this.mountOptionsLayoutPanel);
            this.mountOptionsGroupBox.Location = new System.Drawing.Point(4, 334);
            this.mountOptionsGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mountOptionsGroupBox.Name = "mountOptionsGroupBox";
            this.mountOptionsGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mountOptionsGroupBox.Size = new System.Drawing.Size(624, 97);
            this.mountOptionsGroupBox.TabIndex = 8;
            this.mountOptionsGroupBox.TabStop = false;
            this.mountOptionsGroupBox.Text = "Mount Options";
            // 
            // mountOptionsLayoutPanel
            // 
            this.mountOptionsLayoutPanel.Controls.Add(this.readonlyCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.removableCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.backgroundCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.silentCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.beepCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.explorerCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.askPasswordCheckBox);
            this.mountOptionsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mountOptionsLayoutPanel.Location = new System.Drawing.Point(4, 19);
            this.mountOptionsLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mountOptionsLayoutPanel.Name = "mountOptionsLayoutPanel";
            this.mountOptionsLayoutPanel.Size = new System.Drawing.Size(616, 74);
            this.mountOptionsLayoutPanel.TabIndex = 0;
            // 
            // readonlyCheckBox
            // 
            this.readonlyCheckBox.Location = new System.Drawing.Point(4, 4);
            this.readonlyCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.readonlyCheckBox.Name = "readonlyCheckBox";
            this.readonlyCheckBox.Size = new System.Drawing.Size(133, 30);
            this.readonlyCheckBox.TabIndex = 7;
            this.readonlyCheckBox.Text = "Read Only";
            this.tipInfo.SetToolTip(this.readonlyCheckBox, "Mount volume as read-only.");
            this.readonlyCheckBox.UseVisualStyleBackColor = true;
            this.readonlyCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // removableCheckBox
            // 
            this.removableCheckBox.Location = new System.Drawing.Point(145, 4);
            this.removableCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.removableCheckBox.Name = "removableCheckBox";
            this.removableCheckBox.Size = new System.Drawing.Size(133, 30);
            this.removableCheckBox.TabIndex = 8;
            this.removableCheckBox.Text = "Removable";
            this.tipInfo.SetToolTip(this.removableCheckBox, "Mount volume as removable medium.");
            this.removableCheckBox.UseVisualStyleBackColor = true;
            this.removableCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // backgroundCheckBox
            // 
            this.backgroundCheckBox.Location = new System.Drawing.Point(286, 4);
            this.backgroundCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.backgroundCheckBox.Name = "backgroundCheckBox";
            this.backgroundCheckBox.Size = new System.Drawing.Size(133, 30);
            this.backgroundCheckBox.TabIndex = 9;
            this.backgroundCheckBox.Text = "Background";
            this.tipInfo.SetToolTip(this.backgroundCheckBox, "Automatically perform requested actions and exit (main TrueCrypt window will not " +
        "be displayed).");
            this.backgroundCheckBox.UseVisualStyleBackColor = true;
            this.backgroundCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // silentCheckBox
            // 
            this.silentCheckBox.Location = new System.Drawing.Point(427, 4);
            this.silentCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.silentCheckBox.Name = "silentCheckBox";
            this.silentCheckBox.Size = new System.Drawing.Size(133, 30);
            this.silentCheckBox.TabIndex = 10;
            this.silentCheckBox.Text = "Silent";
            this.tipInfo.SetToolTip(this.silentCheckBox, "Suppresses interaction with the user (prompts, error messages, warnings, etc.).");
            this.silentCheckBox.UseVisualStyleBackColor = true;
            this.silentCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // beepCheckBox
            // 
            this.beepCheckBox.Location = new System.Drawing.Point(4, 42);
            this.beepCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.beepCheckBox.Name = "beepCheckBox";
            this.beepCheckBox.Size = new System.Drawing.Size(133, 30);
            this.beepCheckBox.TabIndex = 11;
            this.beepCheckBox.Text = "Beep";
            this.tipInfo.SetToolTip(this.beepCheckBox, "Beep after a volume has been successfully mounted or dismounted.");
            this.beepCheckBox.UseVisualStyleBackColor = true;
            this.beepCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // explorerCheckBox
            // 
            this.explorerCheckBox.Location = new System.Drawing.Point(145, 42);
            this.explorerCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.explorerCheckBox.Name = "explorerCheckBox";
            this.explorerCheckBox.Size = new System.Drawing.Size(133, 30);
            this.explorerCheckBox.TabIndex = 12;
            this.explorerCheckBox.Text = "Open Explorer";
            this.tipInfo.SetToolTip(this.explorerCheckBox, "Open an Explorer window after a volume has been mounted.");
            this.explorerCheckBox.UseVisualStyleBackColor = true;
            this.explorerCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // askPasswordCheckBox
            // 
            this.askPasswordCheckBox.Location = new System.Drawing.Point(286, 42);
            this.askPasswordCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.askPasswordCheckBox.Name = "askPasswordCheckBox";
            this.askPasswordCheckBox.Size = new System.Drawing.Size(133, 30);
            this.askPasswordCheckBox.TabIndex = 13;
            this.askPasswordCheckBox.Text = "Ask Password";
            this.tipInfo.SetToolTip(this.askPasswordCheckBox, "By unchecking this option the volume gets mounted with help of the AutoType funct" +
        "ion \r\nwith enabled \"Two-Channel Auto-Type Obfuscation\".");
            this.askPasswordCheckBox.UseVisualStyleBackColor = true;
            this.askPasswordCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // volumeTextBox
            // 
            this.volumeTextBox.Location = new System.Drawing.Point(11, 64);
            this.volumeTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.volumeTextBox.Name = "volumeTextBox";
            this.volumeTextBox.Size = new System.Drawing.Size(553, 22);
            this.volumeTextBox.TabIndex = 1;
            this.volumeTextBox.TextChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 45);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Volume File | Partition | Harddrive";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 270);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Drive Letter";
            // 
            // driveComboBox
            // 
            this.driveComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.driveComboBox.FormattingEnabled = true;
            this.driveComboBox.Location = new System.Drawing.Point(12, 289);
            this.driveComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.driveComboBox.Name = "driveComboBox";
            this.driveComboBox.Size = new System.Drawing.Size(160, 24);
            this.driveComboBox.TabIndex = 5;
            this.driveComboBox.SelectionChangeCommitted += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // tipInfo
            // 
            this.tipInfo.AutoPopDelay = 30000;
            this.tipInfo.InitialDelay = 500;
            this.tipInfo.IsBalloon = true;
            this.tipInfo.ReshowDelay = 1000;
            this.tipInfo.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.tipInfo.ToolTipTitle = "Helpful information";
            // 
            // btnReloadDrives
            // 
            this.btnReloadDrives.BackgroundImage = global::KeePassTrueCryptMount.Resources.B16x16_Button_Reload;
            this.btnReloadDrives.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnReloadDrives.Location = new System.Drawing.Point(181, 287);
            this.btnReloadDrives.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnReloadDrives.Name = "btnReloadDrives";
            this.btnReloadDrives.Size = new System.Drawing.Size(47, 30);
            this.btnReloadDrives.TabIndex = 6;
            this.tipInfo.SetToolTip(this.btnReloadDrives, "Refresh free drive letters selection.");
            this.btnReloadDrives.UseVisualStyleBackColor = true;
            this.btnReloadDrives.Click += new System.EventHandler(this.OnReloadDrivesButtonClicked);
            // 
            // hideDialogCheckBox
            // 
            this.hideDialogCheckBox.AutoSize = true;
            this.hideDialogCheckBox.Location = new System.Drawing.Point(129, 454);
            this.hideDialogCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.hideDialogCheckBox.Name = "hideDialogCheckBox";
            this.hideDialogCheckBox.Size = new System.Drawing.Size(161, 21);
            this.hideDialogCheckBox.TabIndex = 15;
            this.hideDialogCheckBox.Text = "Hide dialog next time";
            this.tipInfo.SetToolTip(this.hideDialogCheckBox, "By checking this option the next mount operation starts without\r\nshowing this dia" +
        "log. To show it again, hold the SHIFT key and\r\nclick the \"Mount volume...\" menu " +
        "item.");
            this.hideDialogCheckBox.UseVisualStyleBackColor = true;
            this.hideDialogCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Status:";
            // 
            // stateToolStripLabel
            // 
            this.stateToolStripLabel.Name = "stateToolStripLabel";
            this.stateToolStripLabel.Size = new System.Drawing.Size(49, 17);
            this.stateToolStripLabel.Text = "<State>";
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(12, 449);
            this.btnApply.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(107, 28);
            this.btnApply.TabIndex = 14;
            this.btnApply.Text = "&Apply";
            this.tipInfo.SetToolTip(this.btnApply, "Save settings if you press \"Mount\".\r\nIf you press \"Cancel\", the settings will not" +
        " save.");
            this.btnApply.UseVisualStyleBackColor = true;
            // 
            // tipWarning
            // 
            this.tipWarning.AutoPopDelay = 30000;
            this.tipWarning.InitialDelay = 500;
            this.tipWarning.IsBalloon = true;
            this.tipWarning.ReshowDelay = 1000;
            this.tipWarning.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            this.tipWarning.ToolTipTitle = "Security issue warning";
            // 
            // button1
            // 
            this.button1.Image = global::KeePassTrueCryptMount.Resources.B16x16_Folder_Yellow_Open;
            this.button1.Location = new System.Drawing.Point(570, 60);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(47, 30);
            this.button1.TabIndex = 2;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnFileOpenDialogButtonClicked);
            // 
            // keyFilesTextBox
            // 
            this.keyFilesTextBox.Location = new System.Drawing.Point(8, 232);
            this.keyFilesTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.keyFilesTextBox.Name = "keyFilesTextBox";
            this.keyFilesTextBox.Size = new System.Drawing.Size(560, 22);
            this.keyFilesTextBox.TabIndex = 3;
            this.keyFilesTextBox.TextChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // button2
            // 
            this.button2.Image = global::KeePassTrueCryptMount.Resources.B16x16_Folder_Yellow_Open;
            this.button2.Location = new System.Drawing.Point(574, 228);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(47, 30);
            this.button2.TabIndex = 4;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnFileOpenDialogButtonClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 209);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "Key File(s)";
            // 
            // useUrlFieldCheckBox
            // 
            this.useUrlFieldCheckBox.AutoSize = true;
            this.useUrlFieldCheckBox.Location = new System.Drawing.Point(6, 21);
            this.useUrlFieldCheckBox.Name = "useUrlFieldCheckBox";
            this.useUrlFieldCheckBox.Size = new System.Drawing.Size(121, 21);
            this.useUrlFieldCheckBox.TabIndex = 0;
            this.useUrlFieldCheckBox.Text = "Use &URL Field";
            this.tipInfo.SetToolTip(this.useUrlFieldCheckBox, "Use the URL Field from the main KeePass entry instead of specifying it separately" +
        ".");
            this.useUrlFieldCheckBox.UseVisualStyleBackColor = true;
            this.useUrlFieldCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // volumeFileGroupBox
            // 
            this.volumeFileGroupBox.Controls.Add(this.useUrlFieldCheckBox);
            this.volumeFileGroupBox.Controls.Add(this.volumeTextBox);
            this.volumeFileGroupBox.Controls.Add(this.label1);
            this.volumeFileGroupBox.Controls.Add(this.button1);
            this.volumeFileGroupBox.Location = new System.Drawing.Point(4, 93);
            this.volumeFileGroupBox.Name = "volumeFileGroupBox";
            this.volumeFileGroupBox.Size = new System.Drawing.Size(624, 102);
            this.volumeFileGroupBox.TabIndex = 21;
            this.volumeFileGroupBox.TabStop = false;
            this.volumeFileGroupBox.Text = "Volume File";
            // 
            // TrueCryptMountForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 488);
            this.Controls.Add(this.volumeFileGroupBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.keyFilesTextBox);
            this.Controls.Add(this.btnReloadDrives);
            this.Controls.Add(this.hideDialogCheckBox);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.driveComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.mountOptionsGroupBox);
            this.Controls.Add(this.m_lblSeparator);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.bannerPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TrueCryptMountForm";
            this.Text = "TrueCryptMountForm";
            this.mountOptionsGroupBox.ResumeLayout(false);
            this.mountOptionsLayoutPanel.ResumeLayout(false);
            this.volumeFileGroupBox.ResumeLayout(false);
            this.volumeFileGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel bannerPanel;
        private System.Windows.Forms.Label m_lblSeparator;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox mountOptionsGroupBox;
        private System.Windows.Forms.FlowLayoutPanel mountOptionsLayoutPanel;
        private System.Windows.Forms.CheckBox readonlyCheckBox;
        private System.Windows.Forms.CheckBox removableCheckBox;
        private System.Windows.Forms.TextBox volumeTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox driveComboBox;
        private System.Windows.Forms.ToolTip tipInfo;
        private System.Windows.Forms.CheckBox silentCheckBox;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel stateToolStripLabel;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.CheckBox hideDialogCheckBox;
        private System.Windows.Forms.CheckBox beepCheckBox;
        private System.Windows.Forms.CheckBox backgroundCheckBox;
        private System.Windows.Forms.CheckBox explorerCheckBox;
        private System.Windows.Forms.CheckBox askPasswordCheckBox;
        private System.Windows.Forms.Button btnReloadDrives;
        private System.Windows.Forms.ToolTip tipWarning;
        private System.Windows.Forms.TextBox keyFilesTextBox;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox useUrlFieldCheckBox;
        private System.Windows.Forms.GroupBox volumeFileGroupBox;
    }
}